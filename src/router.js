let pages = {

    // 主站
    main: {
        content: () => import("./main/index.paper"),

        // 免费市场
        "free-market": {
            content: () => import("./main/free-market/index.paper")
        },

        _default_: "free-market"
    },

    _default_: "main"
};

export default {
    install(QuickPaper) {

        // 页面初始化
        QuickPaper.prototype.loadRouter = (doback, deep) => {
            let routers = QuickPaper.urlFormat(window.location.href).router, page = pages;
            for (let i = 0; i < deep; i++) {
                if (!page[routers[i]]) {
                    routers[i] = page['_default_'];
                }
                page = page[routers[i]];
            }
            page.content().then(function (data) {
                doback(data.default, routers);
            });
        };

        // 路由跳转方法
        QuickPaper.prototype.goRouter = (router) => {
            window.location.href = router;
            setTimeout(() => {
                window.location.reload();
            });
        };

    }
};
