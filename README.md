<p align='center'>
    <a href='https://clunch-contrib.github.io/community/' target='_blank'>
        <img src="./view.png">
    </a>
</p>

# community - 社区主站

<p align="center">
    <a href="https://github.com/clunch-contrib/community/graphs/code-frequency" target='_blank'>
        <img alt="GitHub repo size" src="https://img.shields.io/github/repo-size/clunch-contrib/community">
    </a>
    <a href="https://github.com/clunch-contrib/community/graphs/commit-activity" target='_blank'>
        <img alt="GitHub repo commit" src="https://img.shields.io/github/last-commit/clunch-contrib/community">
    </a>
    <a href="https://github.com/clunch-contrib/community" target='_blank'>
        <img alt="GitHub repo stars" src="https://img.shields.io/github/stars/clunch-contrib/community?style=social">
    </a>
</p >

## 如何启动本地编辑？

首先，你需要确保本地安装了node.js，然后，执行下面命令安装项目依赖：

```bash
npm install
```

接着，启动下面命令会自动打开页面，修改内容页面也自动刷新：

```bash
npm run dev
```

如果你想发布开发的代码，执行下面命令进行打包：

```bash
npm run build
```

开源协议
---------------------------------------
[MIT](https://github.com/clunch-contrib/community/blob/master/LICENSE)

Copyright (c) 2021 [hai2007](https://hai2007.gitee.io/sweethome/) 走一步，再走一步。
